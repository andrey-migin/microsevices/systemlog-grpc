﻿using System.ServiceModel;
using System.Threading.Tasks;
using SystemLog.Grpc.Contracts;

namespace SystemLog.Grpc
{
    [ServiceContract(Name = "SystemLog")]
    public interface ISystemLogService
    {
        [OperationContract(Action = "LogEvent")]
        ValueTask<LogEventResponse> RegisterAsync(LogEventRequest request);

        [OperationContract(Action = "GetEvents")]
        ValueTask<GetLogEventsResponse> GetEventsAsync(GetLogEventsRequest request);
    }
    
}