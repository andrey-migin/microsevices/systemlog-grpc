using System;
using System.Runtime.Serialization;

namespace SystemLog.Grpc.Models
{
    
    public enum LogLevel
    {
        Error, Warning, Info
    }
    
    [DataContract]
    public class LogEventModel
    {
        [DataMember(Order = 1)]
        public DateTime DateTime { get; set; }
        
        [DataMember(Order = 2)]
        public LogLevel LogLevel { get; set; }

        [DataMember(Order = 3)]
        public string Component { get; set; }

        [DataMember(Order = 4)]
        public string Process { get; set; }
        
        [DataMember(Order = 5)]
        public string Message { get; set; }

        [DataMember(Order = 6)]
        public string StackTrace { get; set; } 
        

    }
}